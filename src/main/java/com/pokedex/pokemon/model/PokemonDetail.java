package com.pokedex.pokemon.model;

public class PokemonDetail {
    private String name;
    private String url;
    private Abilities[] abilities;
    private Types[] types;
    private Integer weight;
    private Sprites sprites;

    PokemonDetail() {

    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return this.url;
    }

    public Abilities[] getAbilities() {
        return abilities;
    }

    public void setAbilities(Abilities[] abilities) {
        this.abilities = abilities;
    }

    public Types[] getTypes() {
        return types;
    }

    public void setTypes(Types[] types) {
        this.types = types;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public void setSprites(Sprites sprites) {
        this.sprites = sprites;
    }

    public Sprites getSprites() {
        return this.sprites;
    }
}
