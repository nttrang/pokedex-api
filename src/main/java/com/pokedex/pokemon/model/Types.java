package com.pokedex.pokemon.model;

public class Types {

    private Type type;

    Types() {

    }

    public void setType(Type type) {
        this.type = type;
    }

    public Type getType() {
        return this.type;
    }

}
