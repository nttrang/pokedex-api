package com.pokedex.pokemon.model;

public class Sprites {

    private Other other;

    Sprites() {

    }

    public Other getOther() {
        return other;
    }

    public void setOther(Other other) {
        this.other = other;
    }

}
