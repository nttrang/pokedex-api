package com.pokedex.pokemon.model;

public class Description {

    private Integer base_happiness;
    private Integer id;
    private Color color;
    public String name;
    private EggGroups[] egg_groups;
    private Generation generation;
    private EvolutionChain evolution_chain;
    private GrowthRate growth_rate;
    private Habitat habitat;
    private Shape shape;
    private Boolean is_baby;
    private Boolean is_legendary;
    private Boolean is_mythical;

    Description() {
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return this.color;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setEgg_groups(EggGroups[] egg_groups) {
        this.egg_groups = egg_groups;
    }

    public EggGroups[] getEgg_groups() {
        return this.egg_groups;
    }

    public Generation getGeneration() {
        return generation;
    }

    public void setGeneration(Generation generation) {
        this.generation = generation;
    }

    public EvolutionChain getEvolution_chain() {
        return evolution_chain;
    }

    public void setEvolution_chain(EvolutionChain evolution_chain) {
        this.evolution_chain = evolution_chain;
    }

    public GrowthRate getGrowth_rate() {
        return growth_rate;
    }

    public void setGrowth_rate(GrowthRate growth_rate) {
        this.growth_rate = growth_rate;
    }

    public Habitat getHabitat() {
        return habitat;
    }

    public void setHabitat(Habitat habitat) {
        this.habitat = habitat;
    }

    public Shape getShape() {
        return shape;
    }

    public void setShape(Shape shape) {
        this.shape = shape;
    }

    public Boolean getIs_baby() {
        return is_baby;
    }

    public void setIs_baby(Boolean is_baby) {
        this.is_baby = is_baby;
    }

    public Boolean getIs_legendary() {
        return is_legendary;
    }

    public void setIs_legendary(Boolean is_legendary) {
        this.is_legendary = is_legendary;
    }

    public Boolean getIs_mythical() {
        return is_mythical;
    }

    public void setIs_mythical(Boolean is_mythical) {
        this.is_mythical = is_mythical;
    }

    public Integer getBase_happiness() {
        return base_happiness;
    }

    public void setBase_happiness(Integer base_happiness) {
        this.base_happiness = base_happiness;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
