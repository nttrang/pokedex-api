package com.pokedex.pokemon.model;

public class Evolves_to {

    private Species species;

    Evolves_to() {

    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public Species getSpecies() {
        return this.species;
    }

}
