package com.pokedex.pokemon.model;

public class GrowthRate {

    private String name;
    private String url;

    GrowthRate() {

    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return this.url;
    }
}
