package com.pokedex.pokemon.model;

public class Other {

    private Home home;

    Other() {

    }

    public Home getHome() {
        return home;
    }

    public void setHome(Home home) {
        this.home = home;
    }

}
