package com.pokedex.pokemon.model;

public class Chain {

    private EvolvesTo[] evolves_to;
    private Species species;

    Chain() {

    }

    public void setEvolves_to(EvolvesTo[] evolves_to) {
        this.evolves_to = evolves_to;
    }

    public EvolvesTo[] getEvolves_to() {
        return this.evolves_to;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public Species getSpecies() {
        return this.species;
    }

}
