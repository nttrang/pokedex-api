package com.pokedex.pokemon.model;

public class Pokemon {

    private Integer count;
    private String next;
    private String previous;
    private PokemonDetail[] results;

    public Pokemon() {

    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getCount() {
        return this.count;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public String getNext() {
        return this.next;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public String getPrevious() {
        return this.previous;
    }

    public PokemonDetail[] getResults() {
        return results;
    }

    public void setResults(PokemonDetail[] results) {
        this.results = results;
    }

}
