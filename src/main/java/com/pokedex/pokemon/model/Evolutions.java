package com.pokedex.pokemon.model;

public class Evolutions {

    // https://pokeapi.co/api/v2/evolution-chain/1/
    Chain chain;

    Evolutions() {

    }

    public void setChain(Chain chain) {
        this.chain = chain;
    }

    public Chain getChain() {
        return this.chain;
    }
}
