package com.pokedex.pokemon.model;

public class EvolvesTo {

    private Evolves_to[] evolves_to;
    private Species species;

    EvolvesTo() {

    }

    public void setEvolves_to(Evolves_to[] evolves_to) {
        this.evolves_to = evolves_to;
    }

    public Evolves_to[] getEvolves_to() {
        return this.evolves_to;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public Species getSpecies() {
        return this.species;
    }

}