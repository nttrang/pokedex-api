package com.pokedex.pokemon.model;

public class Species {

    private String name;

    Species() {

    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

}
