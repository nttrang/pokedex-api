package com.pokedex.pokemon.model;

public class Abilities {

    private Ability ability;

    Abilities() {

    }

    public Ability getAbility() {
        return ability;
    }

    public void setAbility(Ability ability) {
        this.ability = ability;
    }

}
