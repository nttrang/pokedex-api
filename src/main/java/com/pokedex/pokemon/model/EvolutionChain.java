package com.pokedex.pokemon.model;

public class EvolutionChain {

    private String url;

    EvolutionChain() {

    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return this.url;
    }
}
