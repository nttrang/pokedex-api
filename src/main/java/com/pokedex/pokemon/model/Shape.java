package com.pokedex.pokemon.model;

public class Shape {

    private String name;

    Shape() {

    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

}
