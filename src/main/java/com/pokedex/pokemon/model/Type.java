package com.pokedex.pokemon.model;

public class Type {

    private String name;

    Type() {

    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
