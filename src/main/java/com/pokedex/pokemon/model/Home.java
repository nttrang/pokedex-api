package com.pokedex.pokemon.model;

public class Home {

    private String front_default;

    Home() {

    }

    public String getfront_default() {
        return front_default;
    }

    public void setfront_default(String front_default) {
        this.front_default = front_default;
    }
}
